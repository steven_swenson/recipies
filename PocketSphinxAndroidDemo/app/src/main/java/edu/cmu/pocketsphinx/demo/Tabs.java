package edu.cmu.pocketsphinx.demo;

import android.app.TabActivity;
import android.os.Bundle;

public class Test extends TabActivity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        TabHost tabHost = getTabHost();
        TabHost.TabSpec spec;
        Intent intent;

        intent = new Intent().setClass(this, Tab1Activity.class);
        spec = tabHost.newTabSpec("First").setIndicator("First")
                .setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, Tab2Activity.class);
        spec = tabHost.newTabSpec("Second").setIndicator("Second")
                .setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, Tab3Activity.class);
        spec = tabHost.newTabSpec("Third").setIndicator("Third")
                .setContent(intent);
        tabHost.addTab(spec);


    }
