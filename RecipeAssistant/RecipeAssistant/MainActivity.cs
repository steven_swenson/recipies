﻿using System;
//using System.Threading.Tasks;
using System.Threading;
using System.Timers;
//using System.IO;
using System.Collections.Generic;
using System.Linq;

using Android.App;
using Android.Content;
//using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
//using Android.Util;
using Android.Speech;
using Android.Speech.Tts;
using Android.Graphics;
//using Android.Media;
//using Android.Content.PM;
using Android.Provider;
//using Android.Net;

using Java.IO;

using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;

namespace RecipeAssistant
{
	[Activity (Label = "@string/app_name", MainLauncher = true)]
	public class MainActivity : ListActivity, TextToSpeech.IOnInitListener//, Android.Hardware.Camera.IPictureCallback
	{
		const string Tag = "RecipleAssistant";
		RecipeListAdapter adapter;

		private IMenu menu;

		private RecipeListAdapter.Item recipeItem;
		private Recipe myRecipe;

		public static SpeechRecognitionService speechService;
		System.Timers.Timer speech;
		int speechListenLength;

		TextToSpeech textToSpeech;
		Java.Util.Locale lang;

		AlertDialog addRecipeDialog;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Android.Resource.Layout.ListContent);

			adapter = new RecipeListAdapter (this);
			ListAdapter = adapter;

			myRecipe = new Recipe ();

			this.ListView.ItemLongClick += OnKeyLongPress;

			speechListenLength = 5;

			textToSpeech = new TextToSpeech (this, this, null);

			speech = new System.Timers.Timer();
			speech.Elapsed += new ElapsedEventHandler(pocketsphinx);
			speech.Interval= 10;
			speech.AutoReset = false;

			speechService = new SpeechRecognitionService ();
			Java.Lang.JavaSystem.LoadLibrary ("pocketsphinx_jni");

			CreateDirectoryForPictures ();

			PowerManager powerManager = (PowerManager)GetSystemService (PowerService);
			var wakeLock = powerManager.NewWakeLock (WakeLockFlags.ScreenBright, "wakelock");
			wakeLock.Acquire ();
		}

		protected void OnResume(Bundle bundle){
			if (speechService.IsInitialised ()) {
				toggleVoice = false;
				menu.FindItem (Resource.Id.action_voice).SetTitle (Constants.VoiceDisabled);
			}
		}

		protected override void OnListItemClick (ListView l, View v, int position, long id)
		{
			String itemName = adapter.GetItemName (position);
//			Intent intent = new Intent (ApplicationContext, typeof(RecipeActivity));
//			intent.PutExtra (Constants.RecipeNameToLoad, itemName);
//			StartActivity (intent);
			AddRecipe (itemName);
		}

		protected void OnKeyLongPress (object sender, AdapterView.ItemLongClickEventArgs args){
			var temp = adapter.GetItem (args.Position);
			recipeItem = (RecipeListAdapter.Item)temp;


			PopupMenu menu = new PopupMenu (this, args.View);
			menu.Inflate (Resource.Menu.list_options);

			menu.MenuItemClick += (s1, arg1) => {
				System.Console.WriteLine ("{0} selected", arg1.Item.TitleFormatted);
				if (arg1.Item.TitleFormatted.ToString() == Constants.DeleteRecipe){
					System.Console.WriteLine ("About to Delete");
					RecipeListAdapter.DeleteRecipe(recipeItem.Name);
					adapter = new RecipeListAdapter (this);
					ListAdapter = adapter;
				}
				else if(arg1.Item.TitleFormatted.ToString() == Constants.ModifyRecipe){
					System.Console.WriteLine ("About to Modify");
					//To modify, we should get the recipe from the selected line, and pass the name to AddRecipe.  
					//Then when the addrecipe starts, we get the name from stringExtra and load from file.

					AddRecipe(recipeItem.Name);
				}
			};

			menu.DismissEvent += (s2, arg2) => {
				System.Console.WriteLine ("menu dismissed"); 
			};
			menu.Show ();
		}

		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			MenuInflater.Inflate (Resource.Menu.main, menu);
			this.menu = menu;
			return true;
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {
			case Resource.Id.action_add:
				AddRecipe ();
				return true;
			case Resource.Id.action_voice:
				startVoice ();
				return true;
			case Resource.Id.action_export:
				ExportFile ();
				return true;
			case Resource.Id.action_import:
				ImportFile ();
				return true;
			case Resource.Id.action_search:
				StartSearch ();
				return true;
			}
			return base.OnOptionsItemSelected (item);
		}

		private bool searchFlag = false;
		public void StartSearch(){

			if (searchFlag) {
				searchFlag = false;
				menu.FindItem (Resource.Id.action_search).SetTitle (Constants.Search);

				adapter = new RecipeListAdapter (this);
				ListAdapter = adapter;
			} else {
				searchFlag = true;
				menu.FindItem (Resource.Id.action_search).SetTitle (Constants.SearchClear);
			}

			var alert = new AlertDialog.Builder (this);
			EditText searchTerm= new EditText(this);
			alert.SetView (searchTerm);
			alert.SetTitle ("Search for Recipe by Title");
			alert.SetPositiveButton ("Search", (s, e) => {
				List<string> toc = RecipeListAdapter.GetTableOfContents();
				var resultList = toc.FindAll( delegate(string term) {
					return term.Contains(searchTerm.Text);
				});

				adapter = new RecipeListAdapter (this, resultList);
				ListAdapter = adapter;
			});
			alert.Create ().Show();
		}

		public void ExportFile(){
			var _dir = new Java.IO.File( Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), Constants.AppName.ToString());
			if (!_dir.Exists()) {
				_dir.Mkdirs ();
			}

			var alert = new AlertDialog.Builder (this);
//			EditText newFile= new EditText(this);
//			alert.SetView (newFile);
			alert.SetTitle ("Export a recipe");
			alert.SetMessage ("This will export the last viewed recipe to:\n" + _dir.AbsolutePath + "\nAs: '" + myRecipe.TitleText + ".json'");
			alert.SetPositiveButton ("Export", (s, EventLog) => {
				string dir = System.IO.Path.Combine(_dir.AbsolutePath, myRecipe.TitleText);
				if(!dir.Contains(".json")){
					dir = dir + ".json";
				}
				Java.IO.File file = new Java.IO.File(dir);

				string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
				string filePath = System.IO.Path.Combine(path, myRecipe.TitleText + ".json");
				System.IO.File.Copy(filePath, dir, true);
				Toast.MakeText (this, "File Exported", ToastLength.Short).Show ();
			});
			//			alert.SetView (LayoutInflater.Inflate (Resource.Layout.add_step, null));
			alert.Create ().Show();
		}

		public void nothing(object o, DialogClickEventArgs args){

		}

		public void ImportFile(){
			var _dir = new Java.IO.File( Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), Constants.AppName.ToString());
			if (!_dir.Exists()) {
				_dir.Mkdirs ();
			}

			var alert = new AlertDialog.Builder (this);
//			EditText newFile= new EditText(this);
//			alert.SetView (newFile);
			alert.SetTitle ("Import recipes");
			alert.SetMessage ("This will import all the .json files in the following directory:\n" + _dir.AbsolutePath);
			alert.SetPositiveButton ("Import", (s, EventLog) => {
				string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
				//				string filePath = System.IO.Path.Combine(path, name);

				List<string> tableOfContents = new List<string> ();

				tableOfContents = RecipeListAdapter.GetTableOfContents ();

				List<string> files = System.IO.Directory.GetFiles( _dir.AbsolutePath ).ToList();
//				List<string> fileNames = new List<string>();
				foreach(string file in files){
					if(file.Contains(".json")){
						string tempFile = System.IO.Path.GetFileName(file);
						if (!(tableOfContents.Contains (tempFile))) {
							tableOfContents.Add (tempFile);				
						
							string filePath = System.IO.Path.Combine(path, tempFile);
							string dir = System.IO.Path.Combine(_dir.AbsolutePath, tempFile);
							System.IO.File.Copy(dir, filePath, true);
						}
					}
				}

//				string name = newFile.Text;
//				if(!name.Contains(".json")){
//					name = name + ".json";
//				}
//				string dir = System.IO.Path.Combine(_dir.AbsolutePath, name);
//
//				Java.IO.File file = new Java.IO.File(dir);
//				if(!file.Exists()){
//					Toast.MakeText (this, "Unable to find recipe", ToastLength.Short).Show ();
//					return;
//				}
//
//				//File does exist
//				string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
//				string filePath = System.IO.Path.Combine(path, name);
//				System.IO.File.Copy(dir, filePath, true);
//
//				file = new Java.IO.File(filePath);
//				if(!file.Exists()){
//					Toast.MakeText (this, "Copy failed....sorry", ToastLength.Short).Show ();
//					return;
//				}

				RecipeListAdapter.SaveTableOfContents (tableOfContents);

				RefreshPage();
			});
//			alert.SetView (LayoutInflater.Inflate (Resource.Layout.add_step, null));
			alert.Create ().Show();
		}

		public static Bitmap LoadAndResizeBitmap(string fileName, int width, int height)
		{
			// First we get the the dimensions of the file on disk
			BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
			Bitmap bitmap = BitmapFactory.DecodeFile(fileName, options);

			// Next we calculate the ratio that we need to resize the image by
			// in order to fit the requested dimensions.
			int outHeight = options.OutHeight;
			int outWidth = options.OutWidth;
			int inSampleSize = 1;

			if (outHeight > height || outWidth > width)
			{
				inSampleSize = outWidth > outHeight
					? outHeight / height
					: outWidth / width;
			}

			// Now we will load the image and have BitmapFactory resize it for us.
			options.InSampleSize = inSampleSize;
			options.InJustDecodeBounds = false;
			Bitmap resizedBitmap = BitmapFactory.DecodeFile(fileName, options);

			return resizedBitmap;
		}

		private void DisplayRecipe (Recipe recipe, AlertDialog dialog) {
//			Animation fadeIn = AnimationUtils.LoadAnimation (this, Android.Resource.Animation.FadeIn);
//			titleTextView.Animation = fadeIn;
//			string recipeName;
			//		Recipe recipe;
			ImageView imageView;
			TextView titleTextView, summaryTextView, ingredientsTextView;
			LinearLayout stepsLayout;

			titleTextView = (TextView)dialog.FindViewById (Resource.Id.recipeTextTitle);
			summaryTextView = (TextView)dialog.FindViewById (Resource.Id.recipeTextSummary);
			imageView = (ImageView)dialog.FindViewById (Resource.Id.recipeImageView);
			ingredientsTextView = (TextView)dialog.FindViewById (Resource.Id.textIngredients);
			stepsLayout = (LinearLayout)dialog.FindViewById (Resource.Id.layoutSteps);

			titleTextView.Text = recipe.TitleText;
			summaryTextView.Text = recipe.SummaryText;
			if (recipe.RecipeImage != null) {
//				imageView.Animation = fadeIn;
				int height = 300;
				int width = 300 ;

				Bitmap recipeImage = LoadAndResizeBitmap (recipe.RecipeImage, width, height);
//				Bitmap recipeImage = AssetUtils.LoadBitmapAsset (dialog.Context, recipe.RecipeImage);
				imageView.SetImageBitmap (recipeImage);
			}
			ingredientsTextView.Text = recipe.IngredientsText;

//			FindViewById (Resource.Id.ingredientsHeader).Animation = fadeIn;
//			FindViewById (Resource.Id.ingredientsHeader).Visibility = ViewStates.Visible;
//			FindViewById (Resource.Id.stepsHeader).Animation = fadeIn;

//			FindViewById (Resource.Id.stepsHeader).Animation = fadeIn;

			LayoutInflater inf = LayoutInflater.From (dialog.Context);
			stepsLayout.RemoveAllViews ();
			var stepNumber = 1;
			foreach (Recipe.RecipeStep step in recipe.RecipeSteps) {
				View view = inf.Inflate (Resource.Layout.step_item, null);
				ImageView iv = (ImageView)view.FindViewById (Resource.Id.stepImageView);
				if (step.StepImage == null) {
					iv.Visibility = ViewStates.Gone;
				} else {
					Bitmap stepImage = AssetUtils.LoadBitmapAsset (dialog.Context, step.StepImage);
					iv.SetImageBitmap (stepImage);
				}
				((TextView)view.FindViewById (Resource.Id.textStep)).Text = (stepNumber++) + ". " + step.StepText;
				stepsLayout.AddView(view);
			}
		}

		private void AddRecipe (string recipeToModify = ""){
//			Recipe recipe = new Recipe ();
//			myRecipe = Recipe.FromFile (recipeToModify);
			if (recipeToModify != "") {
				myRecipe = Recipe.FromFile (recipeToModify);
			} else {
				myRecipe = new Recipe ();
			}

			var addRecipe = new AlertDialog.Builder (this);
			if (recipeToModify == "") {
				addRecipe.SetTitle ("Add New Recipe");
			} else {
				addRecipe.SetTitle ("View Recipe");
			}
			addRecipe.SetPositiveButton ("Save", SaveNewRecipe);
			addRecipe.SetNegativeButton ("Cancel", (s, arg) => { });
			addRecipe.SetView (LayoutInflater.Inflate (Resource.Layout.recipe, null));
			addRecipeDialog = addRecipe.Create ();

			addRecipeDialog.Show ();

			DisplayRecipe (myRecipe, addRecipeDialog);

			var btnEditIngr = (Button)addRecipeDialog.FindViewById (Resource.Id.btn_add_ingr);
			var btnEditStep = (Button)addRecipeDialog.FindViewById (Resource.Id.btn_add_step);
			var btnEditTitle = (Button)addRecipeDialog.FindViewById (Resource.Id.btn_edit_title);
			var btnEditSummary = (Button)addRecipeDialog.FindViewById (Resource.Id.btn_edit_summary);
			var btnTakePic = (Button)addRecipeDialog.FindViewById (Resource.Id.btn_add_picture);

			btnEditIngr.Click += (object sender, EventArgs args) => {
//				var parent = (AlertDialog)sender;

				Toast.MakeText (addRecipeDialog.Context, "Editing Ingredients", ToastLength.Short).Show ();
				var ingredients = (TextView)addRecipeDialog.FindViewById(Resource.Id.textIngredients);

				var ingrAlert = new AlertDialog.Builder( addRecipeDialog.Context );
				ingrAlert.SetTitle("Edit Ingredients");
				var editBox = new EditText(addRecipeDialog.Context);
				editBox.SetText(ingredients.Text, TextView.BufferType.Normal);
				ingrAlert.SetView( editBox); 

				ingrAlert.SetPositiveButton("Finish", (s, args2) => {
					Toast.MakeText (this, "Editing Finished", ToastLength.Short).Show ();
//					ingredients.SetText(editBox.Text, TextView.BufferType.Normal);

					myRecipe.IngredientsText = editBox.Text;
					DisplayRecipe (myRecipe, addRecipeDialog);
				});
				ingrAlert.Create().Show();

			};

			btnEditStep.Click += (object sender, EventArgs args) => {
				//				var parent = (AlertDialog)sender;

				Toast.MakeText (addRecipeDialog.Context, "Editing Steps", ToastLength.Short).Show ();

				var editBox = new EditText(addRecipeDialog.Context);
				string stringSteps = "";
				foreach(Recipe.RecipeStep _step in myRecipe.RecipeSteps){
					stringSteps = stringSteps + _step.StepText + '\n';
				}
				if(stringSteps==""){
					editBox.SetText(stringSteps, TextView.BufferType.Normal);
				}
				else{
					editBox.SetText(stringSteps.Substring(0, stringSteps.Length - 1), TextView.BufferType.Normal);
				}

//				var steps = (TextView)dialog.FindViewById(Resource.Id.layoutSteps);

				var stepAlert = new AlertDialog.Builder( addRecipeDialog.Context );
				stepAlert.SetTitle("Edit Steps");

				stepAlert.SetView( editBox); 

				stepAlert.SetPositiveButton("Finish", (s, args2) => {
					Toast.MakeText (this, "Editing Finished", ToastLength.Short).Show ();
//					steps.SetText(editBox.Text, TextView.BufferType.Normal);
					List<Recipe.RecipeStep> recipeSteps = new List<Recipe.RecipeStep>();
					foreach(string _step in editBox.Text.Split('\n')){
						Recipe.RecipeStep tempStep = new Recipe.RecipeStep();
						tempStep.StepText = _step;
						recipeSteps.Add(tempStep);
					}
					myRecipe.RecipeSteps = recipeSteps;
					DisplayRecipe (myRecipe, addRecipeDialog);
				});
				stepAlert.Create().Show();

			};

			btnEditTitle.Click += (object sender, EventArgs args) => {
				//				var parent = (AlertDialog)sender;

				Toast.MakeText (addRecipeDialog.Context, "Edit Title", ToastLength.Short).Show ();
				var title = (TextView)addRecipeDialog.FindViewById(Resource.Id.recipeTextTitle);

				var titleAlert = new AlertDialog.Builder( addRecipeDialog.Context );
				titleAlert.SetTitle("Edit Title");
				var editBox = new EditText(addRecipeDialog.Context);
				editBox.SetText(title.Text, TextView.BufferType.Normal);
				titleAlert.SetView( editBox); 

				titleAlert.SetPositiveButton("Finish", (s, args2) => {
					Toast.MakeText (this, "Editing Finished", ToastLength.Short).Show ();
//					title.SetText(editBox.Text, TextView.BufferType.Normal);
					myRecipe.TitleText = editBox.Text;
					DisplayRecipe (myRecipe, addRecipeDialog);
				});
				titleAlert.Create().Show();

			};

			btnEditSummary.Click += (object sender, EventArgs args) => {
				//				var parent = (AlertDialog)sender;

				Toast.MakeText (addRecipeDialog.Context, "Edit Summary", ToastLength.Short).Show ();
				var summary = (TextView)addRecipeDialog.FindViewById(Resource.Id.recipeTextSummary);

				var summaryAlert = new AlertDialog.Builder( addRecipeDialog.Context );
				summaryAlert.SetTitle("Edit Summary");
				var editBox = new EditText(addRecipeDialog.Context);
				editBox.SetText(summary.Text, TextView.BufferType.Normal);
				summaryAlert.SetView( editBox); 

				summaryAlert.SetPositiveButton("Finish", (s, args2) => {
					Toast.MakeText (this, "Editing Finished", ToastLength.Short).Show ();
//					summary.SetText(editBox.Text, TextView.BufferType.Normal);
					myRecipe.SummaryText = editBox.Text;
					DisplayRecipe (myRecipe, addRecipeDialog);
				});
				summaryAlert.Create().Show();

			};

			btnTakePic.Click += (object sender, EventArgs args) => {
				Toast.MakeText (addRecipeDialog.Context, "Launching Camera.", ToastLength.Short).Show ();

				TakePicture();
//				var _dir = new Java.IO.File( Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), Constants.AppName.ToString());
//				string dir = System.IO.Path.Combine (_dir.AbsolutePath, myRecipe.TitleText+".jpg");
//				CreateDirectoryForPictures();
//
//				Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
//				Java.IO.File photo = new Java.IO.File(dir);
//
//				intent.PutExtra(MediaStore.ExtraOutput,	Uri.FromFile(photo));
//				var imageUri = Uri.FromFile(photo);
//				StartActivityForResult(Intent.CreateChooser(intent, "Capture Image"), 1);
//
//				myRecipe.RecipeImage = dir;
			};
				
		}

		public void TakePicture(){
			var _dir = new Java.IO.File( Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), Constants.AppName.ToString());
			string dir = System.IO.Path.Combine (_dir.AbsolutePath, myRecipe.TitleText+".jpg");
			CreateDirectoryForPictures();

			Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
			Java.IO.File photo = new Java.IO.File(dir);

			intent.PutExtra(MediaStore.ExtraOutput,	Uri.FromFile(photo));
			var imageUri = Uri.FromFile(photo);
			StartActivityForResult(Intent.CreateChooser(intent, "Capture Image"), 1);

			myRecipe.RecipeImage = dir;
		}

		private void SaveNewRecipe(object o = null, DialogClickEventArgs args = null){
			Toast.MakeText (this, "Saving Recipe", ToastLength.Short).Show ();

			RecipeListAdapter.SaveRecipe (myRecipe);

			List<string> tableOfContents = new List<string> ();

			tableOfContents = RecipeListAdapter.GetTableOfContents ();

			string newName = myRecipe.TitleText + ".json";
			if (!(tableOfContents.Contains (newName))) {
				tableOfContents.Add (myRecipe.TitleText + ".json");
			}

			RecipeListAdapter.SaveTableOfContents (tableOfContents);

			//TODO: Rename the image with the new title

			adapter = new RecipeListAdapter (this);
			ListAdapter = adapter;
		}
			
		private void CreateDirectoryForPictures()
		{
//			var _dir = Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures);
			var _dir = new Java.IO.File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), Constants.AppName);
//			string dir = System.IO.Path.Combine (_dir.AbsolutePath, Constants.AppName.ToString());

			if (_dir.Exists ()) {
				return;
			} else {
				_dir.Mkdirs ();
			}
		}

		private bool toggleVoice = true;

		private void startVoice(){
//			toggleVoice = !toggleVoice;

			if (toggleVoice) {
				toggleVoice = false;
				menu.FindItem (Resource.Id.action_voice).SetTitle (Constants.VoiceEnabled);

				speechService.Init ();
				Toast.MakeText (this, "Voice Services Activated", ToastLength.Short).Show ();

//				speechService.enableListening ();
				speech.Start ();

			} else {
				toggleVoice = true;
				menu.FindItem (Resource.Id.action_voice).SetTitle (Constants.VoiceDisabled);
				speechService.stopListening ();
				speech.Stop ();
				Toast.MakeText (this, "Voice Services Deactivated", ToastLength.Short).Show ();
//				StopService (new Intent (this, typeof(voiceService)));
			}
		}

		private async void pocketsphinx (object source, ElapsedEventArgs e){
			speech.Stop ();
//			if (!toggleVoice) {
//				speechService.stopListening ();
//				return;
//			}

			System.Console.WriteLine ("Start sphinx right here");
			var result = await speechService.ListenAsync (speechListenLength).ConfigureAwait(true);

			System.Console.WriteLine ("Returned from await");
			if (result == Constants.KEYPHRASE) {
				if (weirdSetup == true) {
					weirdSetup = false;
					speech.Start ();
				} else {
					weirdSetup = true;
					speech.Stop ();
					recordVoice (-1);
				}
			} else {
				if (toggleVoice == false) {
					speech.Start ();
				}
			}
		}

		private bool weirdSetup = false;
		private const int VOICE = 10;

		private void recordVoice (int status = -1)
		{
			speechService.stopListening ();
			speech.Stop ();

			var voiceIntent = new Intent (RecognizerIntent.ActionRecognizeSpeech);
			voiceIntent.PutExtra (RecognizerIntent.ExtraLanguageModel, RecognizerIntent.LanguageModelFreeForm);
			voiceIntent.PutExtra (RecognizerIntent.ExtraPrompt, "Speak Now");
			voiceIntent.PutExtra (RecognizerIntent.ExtraSpeechInputCompleteSilenceLengthMillis, 500);
			voiceIntent.PutExtra (RecognizerIntent.ExtraSpeechInputPossiblyCompleteSilenceLengthMillis, 500);
			voiceIntent.PutExtra (RecognizerIntent.ExtraSpeechInputMinimumLengthMillis, 500);
			voiceIntent.PutExtra (RecognizerIntent.ExtraMaxResults, 1);
			voiceIntent.PutExtra (RecognizerIntent.ExtraLanguage, Java.Util.Locale.Default);
			if (status == -1) {
				Speak ("Listening");
			}


			if (status == -1) {
				StartActivityForResult (voiceIntent, VOICE);
			} else {
				StartActivityForResult (voiceIntent, status);
			}
		}

//		public bool startListening = true;
		protected override void OnActivityResult(int requestCode, Result resultVal, Intent data)
		{
			speech.Stop ();
			speechService.stopListening ();
			base.OnActivityResult(requestCode, resultVal, data);
			if (requestCode == VOICE) {
				if (resultVal == Result.Ok) {
					var matches = data.GetStringArrayListExtra (RecognizerIntent.ExtraResults);
					Toast.MakeText (this, matches [0].ToString (), ToastLength.Short).Show ();
					System.Console.WriteLine ("Voice to speech: {0}", matches [0]);
//					Speak (matches [0], QueueMode.Flush, null);
					//					var result = FindViewById<TextView>(Resource.Id.ResultText);
					//					result.Text = matches[0].ToString();

					MyParser (matches [0]);
//					speech.Start ();
				} else {
					if (toggleVoice == true) {
						speech.Start ();
					}
				}


			} else if (requestCode == 0) {
				adapter = new RecipeListAdapter (this);
				ListAdapter = adapter;
			} else if (requestCode == 1) {
				//Return from camera

//				Bundle extras = data.GetBundleExtra("data");
//				System.Console.WriteLine (data.Extras.KeySet ().ToString ());

				DisplayRecipe (myRecipe, addRecipeDialog);
			} else {
				if (resultVal == Result.Ok) {
					var matches = data.GetStringArrayListExtra (RecognizerIntent.ExtraResults);
					Toast.MakeText (this, matches [0].ToString (), ToastLength.Short).Show ();
					System.Console.WriteLine ("Google returned a string of {0}", matches [0]);
					MyParser (matches [0], requestCode);
				}
			} 


		}

		// Interface method required for IOnInitListener
		void TextToSpeech.IOnInitListener.OnInit(OperationResult status)
		{
			lang = Java.Util.Locale.Default;
			// if we get an error, default to the default language
			if (status == OperationResult.Error)
				textToSpeech.SetLanguage(Java.Util.Locale.Default);
			// if the listener is ok, set the lang
			if (status == OperationResult.Success)
				textToSpeech.SetLanguage(lang);
		}

//		public bool firstTime = true;
		public void MyParser(string command, int subStatus = -1){
			string cmd = command.ToLower ();
			if (cmd.Contains("cancel") || cmd.Contains("nevermind")) {
				if (toggleVoice == true) {
					speech.Start ();
				}
				return;
			}

			if (subStatus == 16) {
				//Add Step
				Recipe.RecipeStep newStep = new Recipe.RecipeStep ();
				newStep.StepText = command;
				myRecipe.RecipeSteps.Add (newStep);
				DisplayRecipe (myRecipe, addRecipeDialog);
			} else if (subStatus == 17) {
				myRecipe.IngredientsText = myRecipe.IngredientsText + "\n" + command;
				DisplayRecipe (myRecipe, addRecipeDialog);
			} else if (subStatus == 18) {
				myRecipe.TitleText = command;
				DisplayRecipe (myRecipe, addRecipeDialog);

			} else if (subStatus == 19) {
				myRecipe.SummaryText = command;
				DisplayRecipe (myRecipe, addRecipeDialog);
			} else if (subStatus == 20) {
				int temp = String2Num (cmd);
				myRecipe.RecipeSteps.RemoveAt (temp);
				DisplayRecipe (myRecipe, addRecipeDialog);
			} else if (subStatus == 21) {
				int temp = String2Num (cmd);
				List<string> tempIngrs = myRecipe.IngredientsText.Split ('\n').ToList ();
				tempIngrs.RemoveAt (temp - 1);
				myRecipe.IngredientsText = String.Join ("\n", tempIngrs);
				//search ingredients for ingredient, then remove that line
//				List<string> tempIngrs = new List<string> ();
//				foreach (string ingr in myRecipe.IngredientsText.Split('\n')) {
//					if (ingr.Contains (cmd)) {
//						continue;
//					}
//					tempIngrs.Add (ingr);
//				}
				myRecipe.IngredientsText = tempIngrs.ToString ();
				DisplayRecipe (myRecipe, addRecipeDialog);
			} else if (subStatus == 22) {
				//modify the step
				int temp = String2Num (cmd);
				myRecipe.RecipeSteps [temp].StepText = "modify";

				Speak ("What should the new step say?");
				recordVoice (23);
				return;
			} else if (subStatus == 23) {
				foreach (Recipe.RecipeStep _step in myRecipe.RecipeSteps) {
					if (_step.StepText == "modify") {
						_step.StepText = cmd;
						DisplayRecipe (myRecipe, addRecipeDialog);
						break;
					}
				}
			} else if (subStatus == 24) {
				List<string> tempIngr = myRecipe.IngredientsText.Split ('\n').ToList ();
				for (int i = 0; i < tempIngr.Count; i++) {
					if (tempIngr [i].Contains (cmd)) {
						tempIngr [i] = "modify";
					} 
				}
				myRecipe.IngredientsText = String.Join ("\n", tempIngr);
				Speak ("Say the new ingredient");
				recordVoice (25);
				return;
			} else if (subStatus == 25) {
				List<string> tempIngr = myRecipe.IngredientsText.Split ('\n').ToList ();
				for (int i = 0; i < tempIngr.Count; i++) {
					if (tempIngr [i] == "modify") {
						tempIngr [i] = cmd;
						break;
					}
				}
				myRecipe.IngredientsText = String.Join ("\n", tempIngr);
				DisplayRecipe (myRecipe, addRecipeDialog);
			} else if (subStatus == 26) {
				List<string> toc = RecipeListAdapter.GetTableOfContents ();
				List<string> resultList = new List<string> ();
				var words = cmd.Split (' ').ToList<string> ();
				for (int i = 0; i < toc.Count; i++) {
					for (int j = 0; j < words.Count; j++) {
						if (toc [i].ToLower().Contains (words [j])) {
							resultList.Add (toc [i]);
							break;
						}
					}
				}
//				
				if (resultList.Count > 0) {
					Speak ("Showing " + System.IO.Path.GetFileNameWithoutExtension(resultList [0]));
					AddRecipe (resultList [0]);
				} else {
					Speak ("No recipe found that matches that name");
				}
			} else if (subStatus == 27) {
				menu.FindItem (Resource.Id.action_search).SetTitle (Constants.SearchClear);

				List<string> toc = RecipeListAdapter.GetTableOfContents ();
				List<string> resultList = new List<string> ();
				var words = cmd.Split (' ').ToList<string> ();
				for (int i = 0; i < toc.Count; i++) {
					for (int j = 0; j < words.Count; j++) {
						if (toc [i].ToLower().Contains (words [j])) {
							resultList.Add (toc [i]);
							break;
						}
					}
				}
				//				
				if (resultList.Count > 0) {
//					Speak ("Showing " + System.IO.Path.GetFileNameWithoutExtension(resultList [0]));
//					AddRecipe (resultList [0]);
					adapter = new RecipeListAdapter (this, resultList);
					ListAdapter = adapter;
					searchFlag = true;
					menu.FindItem (Resource.Id.action_search).SetTitle (Constants.SearchClear); 
				} else {
					Speak ("No recipes found");
				}

//				List<string> toc = RecipeListAdapter.GetTableOfContents ();
//				var resultList = toc.FindAll (delegate(string term) {
//					return term.Contains (cmd);
//				});
//
//				adapter = new RecipeListAdapter (this, resultList);
//				ListAdapter = adapter;
			}
			else if(subStatus == 28){
				int numStep = String2Num (cmd);
				int speakStep = numStep + 1;
				Speak ("Reading step " + speakStep.ToString ());

				Speak (myRecipe.RecipeSteps [numStep].StepText);
			}



			if (cmd.Contains ("add") || cmd.Contains ("set") || cmd.Contains ("make") || cmd.Contains("new") || subStatus == 12) {
				if (cmd.Contains ("recipe")) {
					AddRecipe ();
				} else if (cmd.Contains ("title")) {
					if (addRecipeDialog.IsShowing) {
						Speak ("Say the title of the recipe");
						recordVoice (18);
						return;
					} else {
						Speak ("Please select a recipe.");
					}
				} else if (cmd.Contains ("summary")) {
					if (addRecipeDialog.IsShowing) {
						Speak ("Say the recipe summary");
						recordVoice (19);
						return;
					} else {
						Speak ("Please select a recipe.");
					}
				} else if (cmd.Contains ("step") || cmd.Contains ("stuff") || cmd.Contains ("sap")|| cmd.Contains ("set")) {
					if (addRecipeDialog.IsShowing) {
						Speak ("Say the step");
						recordVoice (16);
					} else {
						Speak ("Please select a recipe.");
					}
					return;
				} else if (cmd.Contains ("ingredient")) {
					if (addRecipeDialog.IsShowing) {
						Speak ("Say the ingredient");
						recordVoice (17);
						return;
					} else {
						Speak ("Please select a recipe.");
					}
				
				} else if (cmd.Contains ("picture")) {
					if (addRecipeDialog.IsShowing) {
						TakePicture ();
					} else {
						Speak ("Please select a recipe.");
					}
				} else {
					if (subStatus == -1) {
						Speak ("I didn't understand.  Please say it again.");
//						recordVoice (12);
//						return;
					} 
				}
			} else if (cmd.Contains ("take") && cmd.Contains("picture")) {
				if (addRecipeDialog.IsShowing) {
					TakePicture ();
				}
			} else if (cmd.Contains ("remove") || cmd.Contains ("delete") || subStatus == 13) {
				if (addRecipeDialog.IsShowing) {
					if (cmd.Contains ("recipe")) {
						Speak ("Deleting the current recipe");
						RecipeListAdapter.DeleteRecipe (myRecipe.TitleText + ".json");
						RefreshPage ();
					} else if (cmd.Contains ("step") || cmd.Contains ("stuff") || cmd.Contains ("sap") || cmd.Contains ("set")) {
						Speak ("Say the step number to delete");
						recordVoice (20);
						return;
					} else if (cmd.Contains ("ingredient")) {
						Speak ("Say the ingredient number to delete");
						recordVoice (21);
						return;
					} else if (cmd.Contains ("title")) {
						Speak ("Removing title");
						myRecipe.TitleText = "";
						DisplayRecipe (myRecipe, addRecipeDialog);
					} else if (cmd.Contains ("summary")) {
						Speak ("Removing summary");
						myRecipe.SummaryText = "";
						DisplayRecipe (myRecipe, addRecipeDialog);
					} else {
						if (subStatus == -1) {
							Speak ("I didn't understand.  Please say it again.");
						}
					}
				} else {
					Speak ("Please choose a recipe first.");
				}
			} else if (cmd.Contains ("save")) {
				if (addRecipeDialog.IsShowing) {
					Speak ("Saving");
					SaveNewRecipe ();
				} else {
					Speak ("Please choose a recipe first.");
				}
			} else if (cmd.Contains ("modify") || cmd.Contains ("change")) {
				if (addRecipeDialog.IsShowing) {
					if (cmd.Contains ("title")) {
						Speak ("Say the new title");
						recordVoice (18);
						return;
					} else if (cmd.Contains ("summary")) {
						Speak ("Say the new summary");
						recordVoice (19);
						return;
					} else if (cmd.Contains ("step") || cmd.Contains("stuff") || cmd.Contains ("sap") || cmd.Contains ("set")) {
						Speak ("Say the step number to modify");
						recordVoice (22);
						return;
					} else if (cmd.Contains ("ingredient")) {
						Speak ("Say the ingredient to modify");
						recordVoice (24);
						return;
					} else {
						if (subStatus == -1) {
							Speak ("I didn't understand.  Please say it again.");
//						recordVoice (14);
//						return;
						}
					}
				} else {
					Speak ("Please choose a recipe first.");
				}
			} else if (cmd.Contains ("read") || cmd.Contains("tell") || subStatus == 15) {
				if (addRecipeDialog.IsShowing) {

					if (cmd.Contains ("step") || cmd.Contains("stuff") || cmd.Contains ("sap") || cmd.Contains ("set")) {
						cmd = cmd.Replace ("step", "");
						int stepNum = String2Num (cmd);
						if (stepNum != -1) {
							Speak (myRecipe.RecipeSteps [stepNum].StepText);
						} else {
							Speak ("Please say the step to read");
							recordVoice (28);
							return;
						}
					} else if (cmd.Contains ("ingredient")) {
						cmd = cmd.Replace ("ingredient", "");
						bool noIngredientFound = true;
						foreach (string ingr in myRecipe.IngredientsText.Split('\n')) {
							if (ingr.Contains (cmd)) {
								noIngredientFound = false;
								Speak (ingr);
								break;			
							}
						}
						if (noIngredientFound) {
							foreach (string ingr in myRecipe.IngredientsText.Split('\n')) {
								Speak (ingr);
								Speak (" ");
							}
						}
					} else if (cmd.Contains ("all") || cmd.Contains ("entire") || cmd.Contains("recipe")) {
						if (cmd.Contains ("recipe")) {
							Speak (myRecipe.TitleText);
							Speak (" ");
							Speak (myRecipe.SummaryText);
							Speak (" ");
							Speak ("Ingredients:");
							foreach (string ingr in myRecipe.IngredientsText.Split('\n')) {
								Speak (ingr);
								Speak (" ");
							}

							Speak ("Steps:");
							foreach (Recipe.RecipeStep step in myRecipe.RecipeSteps) {
								Speak (step.StepText);
								Speak (" ");
							}
						} else if (cmd.Contains ("step") || cmd.Contains ("stuff") || cmd.Contains ("sap") || cmd.Contains ("set")) {
							Speak ("Steps:");
							foreach (Recipe.RecipeStep step in myRecipe.RecipeSteps) {
								Speak (step.StepText);
								Speak (" ");
							}
						} else if (cmd.Contains ("ingredient")) {
							Speak ("Ingredients:");
							foreach (string ingr in myRecipe.IngredientsText.Split('\n')) {
								Speak (ingr);
								Speak (" ");
							}
						}
					} else if (cmd.Contains ("title")) {
						Speak (myRecipe.TitleText);
					} else if (cmd.Contains ("summary")) {
						Speak (myRecipe.SummaryText);
					} else {
						if (subStatus == -1) {
							Speak ("I didn't understand.  Please say it again.");
//						recordVoice (15);
//						return;
						}
					}
				} else {
					Speak ("Please choose a recipe first");
				}
			} else if (cmd.Contains ("clear")) {
				searchFlag = false;
				menu.FindItem (Resource.Id.action_search).SetTitle (Constants.Search);

				adapter = new RecipeListAdapter (this);
				ListAdapter = adapter;
			} else if (cmd.Contains ("search")) {
				Speak ("Say the name of the recipe you're looking for");
				recordVoice (27);
				return;
			} else if (cmd.Contains ("select") || cmd.Contains ("choose") || cmd.Contains ("open")) {
				Speak ("Say the name of the recipe to load.");
				recordVoice (26);
				return;
			} else if (cmd.Contains ("joke")) {
				Speak (jokes._jokes [new Random ().Next (jokes._jokes.Count)]);
			} else if (cmd.Contains ("disable") && cmd.Contains ("voice")) {
				Speak ("Disabling Voice Commands");
				toggleVoice = true;
				startVoice ();
			} else if (cmd.Contains ("close")) {
				addRecipeDialog.Cancel ();
			}else {
				if (subStatus == -1) {
					Speak ("I didn't understand.  Please say it again.");
//					recordVoice (11);
//					return;
				} 
			}
			if (toggleVoice == false) {
				speech.Start ();
			}
		}

		public void Speak(string stringToSpeak){
			textToSpeech.Speak (stringToSpeak, QueueMode.Flush, null);
			while (textToSpeech.IsSpeaking) {
				Thread.Sleep (100);
			}
//			Thread.Sleep (200);
		}

		public void RefreshPage(){
			adapter = new RecipeListAdapter (this);
			ListAdapter = adapter;
		}

		public int String2Num(string listString){
			string[] a = new string[] { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", 
				"eleven", "twelve", "thirteen", "fourteen", "fifteen", "fixteen", "seventeen", "eighteen", "nineteen", "to"};
			string[] b = new string[] {
				"1",
				"2",
				"3",
				"4",
				"5",
				"6",
				"7",
				"8",
				"9",
				"10",
				"11",
				"12",
				"13",
				"14",
				"15",
				"16",
				"17",
				"18",
				"19",
				"2"
			};

			foreach(string word in listString.Split(' ')){
				for (int i = 0; i < a.Length; i++)
				{
					if (word.Contains (a [i]) || word.Contains (b [i])) {
						return Convert.ToInt32(b[i]);
					}
				}
			}
			return -1;
		}
	}
}

