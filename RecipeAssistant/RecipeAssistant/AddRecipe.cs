﻿using System;
using System.Collections.Generic;
using System.Timers;
using System.Text;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Speech.Tts;
using Android.Speech;

using Newtonsoft.Json;

namespace RecipeAssistant
{
	[Activity (Label = "@string/app_add")]			
	public static class AddRecipe 
	{
		static IMenu menu;

		static List<string> steps;
		static List<string> ingrs;

		static List<Recipe.RecipeStep> recipeStep;

		static IListAdapter stepList;
		static IListAdapter ingrList;

		static Recipe recipeToModify;

		static int selectedListView;
		static int selectedItem;

		static SpeechRecognitionService speechService;
		static System.Timers.Timer speech;
		static int speechListenLength;

		static TextToSpeech textToSpeech;
		static Java.Util.Locale lang;

		public static void On_Create (Bundle bundle)
		{
//			base.OnCreate (bundle);
//			SetContentView (Resource.Layout.add_recipe);

			// Create your application here
			steps = new List<string>();
			ingrs = new List<string>();

			recipeStep = new List<Recipe.RecipeStep>();

//			Button btn_add_step = FindViewById<Button>(Resource.Id.btn_add_step);
//			Button btn_add_ingr = FindViewById<Button>(Resource.Id.btn_add_ingr);
//			Button btn_save = FindViewById<Button>(Resource.Id.btn_save);

//			btn_add_step.Click += (o, e) => { addStep(o,e); };
//			btn_add_ingr.Click += (o, e) => { addIngr(o,e); };
//			btn_save.Click += (o, e) => { saveRecipe(o,e); };

//			stepList = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, steps);
//			var step_list = FindViewById<ListView> (Resource.Id.list_step);
			step_list.Adapter = stepList;
			step_list.ItemLongClick += OnKeyLongPress;

//			ingrList = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, ingrs);
//			var ingr_list = FindViewById<ListView> (Resource.Id.list_ingr);
			ingr_list.Adapter = ingrList;
			ingr_list.ItemLongClick += OnKeyLongPress;

//			speechService = Intent.GetStringExtra ("speech");
			speechService = MainActivity.speechService;

			speechListenLength = 5;

//			textToSpeech = new TextToSpeech (this, this, null);

			speech = new System.Timers.Timer();
			speech.Elapsed += new ElapsedEventHandler(pocketsphinx);
			speech.Interval= 10;
			speech.AutoReset = false;

//			if (Intent.GetStringExtra ("modify") != "") {
//				string recipeName = Intent.GetStringExtra ("modify");

				recipeToModify = RecipeListAdapter.GetRecipe (recipeName);

				string[] ingr = recipeToModify.IngredientsText.Split ('\n');
				foreach (string _ingr in ingr) {
					ingrs.Add (_ingr);
				}
//				ingrList = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, ingrs);
//				ingr_list = FindViewById<ListView> (Resource.Id.list_ingr);
				ingr_list.Adapter = ingrList;

				var temp = recipeToModify.RecipeSteps;
				for (int i = 0; i < temp.Count; i++) {
					steps.Add (temp [i].StepText);
				}
//				stepList = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, steps);
//				step_list = FindViewById<ListView> (Resource.Id.list_step);
				step_list.Adapter = stepList;

//				var name = (EditText)FindViewById (Resource.Id.txt_title);
				name.SetText (recipeToModify.TitleText, EditText.BufferType.Editable);


//				var summary = (EditText)FindViewById (Resource.Id.txt_summary);
				summary.SetText (recipeToModify.SummaryText, EditText.BufferType.Editable);
			}

		}

		public static void OnKeyLongPress (object sender, AdapterView.ItemLongClickEventArgs args){
			var temp = args.Parent.GetItemAtPosition (args.Position);
//			if (args.Parent == FindViewById<ListView> (Resource.Id.list_step)) {
//				selectedListView = 0;
//			} else if(args.Parent == FindViewById<ListView> (Resource.Id.list_ingr)) {
//				selectedListView = 1;
//			}
			selectedItem = args.Position;

//			PopupMenu menu = new PopupMenu (this, args.View);
			menu.Inflate (Resource.Menu.list_options);

			menu.MenuItemClick += (s1, arg1) => {
				Console.WriteLine ("{0} selected", arg1.Item.TitleFormatted);
				if (arg1.Item.TitleFormatted.ToString() == Constants.DeleteRecipe){
					Console.WriteLine ("About to Delete");
					if(selectedListView == 0){
						//Delete from Steps
						steps.Remove(steps[selectedItem]);

//						stepList = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, steps);
//						var step_list = FindViewById<ListView> (Resource.Id.list_step);
						step_list.Adapter = stepList;
					}
					else if (selectedListView == 1){
						//Delete from Ingrs
						ingrs.Remove(ingrs[selectedItem]);

//						ingrList = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, ingrs);
//						var ingr_list = FindViewById<ListView> (Resource.Id.list_ingr);
						ingr_list.Adapter = ingrList;
					}
				}
				else if(arg1.Item.TitleFormatted.ToString() == Constants.ModifyRecipe){
					Console.WriteLine ("About to Modify");
					if(selectedListView == 0){
						//Modify Steps
						var alert = new AlertDialog.Builder (this);
						alert.SetTitle ("Modify Step");
						alert.SetMessage ("Modify a step to this recipe");
						alert.SetPositiveButton("Modify", modifyStep);
						alert.SetView (LayoutInflater.Inflate (Resource.Layout.add_step, null));
						alert.Create ().Show();
					}
					else if (selectedListView == 1){
						//Modify Ingrs
						var alert = new AlertDialog.Builder (this);
						alert.SetTitle ("Modify Ingredient");
						alert.SetMessage ("Modify an ingredient to this recipe");
						alert.SetPositiveButton("Modify", modifyIngr);
						alert.SetView (LayoutInflater.Inflate (Resource.Layout.add_ingr, null));
						alert.Create ().Show();
					}
				}
			};

			menu.DismissEvent += (s2, arg2) => {
				Console.WriteLine ("menu dismissed"); 
			};
			menu.Show ();
		}

		public static void addStep(object o, EventArgs e){
			Toast.MakeText (this, "You pressed Add Step", ToastLength.Short).Show ();
			var alert = new AlertDialog.Builder (this);
			alert.SetTitle ("Add Step");
			alert.SetMessage ("Add a step to this recipe");
			alert.SetPositiveButton("Add", saveStep);
			alert.SetView (LayoutInflater.Inflate (Resource.Layout.add_step, null));
			alert.Create ().Show();

			//			Button btn_save_step = FindViewById<Button> (Resource.Id.btn_save_step);
			//			btn_save_step.Click += (o1, e1) => { saveStep(o1,e1); };
		}

		public static void addIngr(object o, EventArgs e){
			Toast.MakeText (this, "You pressed Add Ingredient", ToastLength.Short).Show ();
			var alert = new AlertDialog.Builder (this);
			alert.SetTitle ("Add Ingredient");
			alert.SetMessage ("Add an ingredient to this recipe");
			alert.SetPositiveButton("Add", saveIngr);
			alert.SetView (LayoutInflater.Inflate (Resource.Layout.add_ingr, null));
			alert.Create ().Show();

			//			Button btn_save_ingr = FindViewById<Button> (Resource.Id.btn_save_ingr);
			//			btn_save_ingr.Click += (o1, e1) => { saveIngr(o1,e1); };
		}

		public static void saveRecipe(object o, EventArgs e){
			Toast.MakeText (this, "You pressed Save", ToastLength.Short).Show ();

			var name = (EditText)FindViewById (Resource.Id.txt_title);
			var summary = (EditText)FindViewById (Resource.Id.txt_summary);

			Recipe recipe = new Recipe ();
			recipe.TitleText = name.Text;
			recipe.SummaryText = summary.Text;
			recipe.IngredientsText = string.Join (" ", ingrs.ToArray ());
			recipe.RecipeSteps = recipeStep;

			RecipeListAdapter.SaveRecipe (recipe);

			List<string> tableOfContents = new List<string> ();

			tableOfContents = RecipeListAdapter.GetTableOfContents ();

			string newName = name.Text + ".json";
			if (!(tableOfContents.Contains (newName))) {
				tableOfContents.Add (name.Text + ".json");
			}

			RecipeListAdapter.SaveTableOfContents (tableOfContents);

			Finish ();
		}

		public static void test(object o, EventArgs e){
			Toast.MakeText (this, "Beep Boop", ToastLength.Short).Show ();
		}

		public static void saveStep(object sender, DialogClickEventArgs args){
			var dialog = (AlertDialog)sender;
			var step = (EditText)dialog.FindViewById (Resource.Id.txt_step);

			Toast.MakeText (this, step.Text, ToastLength.Short).Show ();

			Recipe.RecipeStep recipe = new Recipe.RecipeStep();
			recipe.StepText = step.Text;
			recipeStep.Add (recipe);

			steps.Add (step.Text);
			stepList = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, steps);

			var step_list = FindViewById<ListView> (Resource.Id.list_step);
			step_list.Adapter = stepList;
		}

		public static void modifyStep(object sender, DialogClickEventArgs args){
			var dialog = (AlertDialog)sender;
			var step = (EditText)dialog.FindViewById (Resource.Id.txt_step);

			Toast.MakeText (this, step.Text, ToastLength.Short).Show ();

			steps[selectedItem] = step.Text + "\n";
			stepList = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, steps);

			var step_list = FindViewById<ListView> (Resource.Id.list_step);
			step_list.Adapter = stepList;
		}

		public static void saveIngr(object sender, DialogClickEventArgs args){
			var dialog = (AlertDialog)sender;
			var ingredient = (EditText)dialog.FindViewById (Resource.Id.txt_ingr);

			Toast.MakeText (this, ingredient.Text, ToastLength.Short).Show ();

			ingrs.Add (ingredient.Text + "\n");
			ingrList = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, ingrs);

			var ingr_list = FindViewById<ListView> (Resource.Id.list_ingr);
			ingr_list.Adapter = ingrList;
		}

		public static void modifyIngr(object sender, DialogClickEventArgs args){
			var dialog = (AlertDialog)sender;
			var ingredient = (EditText)dialog.FindViewById (Resource.Id.txt_ingr);

			Toast.MakeText (this, ingredient.Text, ToastLength.Short).Show ();

			ingrs[selectedItem] = ingredient.Text + "\n";
			ingrList = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, ingrs);

			var ingr_list = FindViewById<ListView> (Resource.Id.list_ingr);
			ingr_list.Adapter = ingrList;
		}

		public static bool On_CreateOptionsMenu (IMenu menu)
		{
//			MenuInflater.Inflate (Resource.Menu.main, menu);
//			this.menu = menu;

			if (speechService.IsInitialised ()) {
				toggleVoice = false;
				menu.FindItem (Resource.Id.action_voice).SetTitle (Constants.VoiceDisabled);
			}

			return true;
		}

		public static bool On_OptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {
			case Resource.Id.action_add:
//				Toast.MakeText (this, "That's where you are now!", ToastLength.Short).Show ();
				return true;
			case Resource.Id.action_voice:
				startVoice ();
				return true;
				//			case Resource.Id.action_set_threshold:
				//				return true;
			case Resource.Id.action_set_listen_time:
				SetListenTime ();
				return true;
			}
//			return base.OnOptionsItemSelected (item);
		}

		public static void SetListenTime(){
//			var alert = new AlertDialog.Builder (this);
//			NumberPicker threshold = new NumberPicker (this);
			threshold.MaxValue = 20;
			threshold.MinValue = 2;
			alert.SetView (threshold);
			alert.SetTitle ("Set Listen Time");
			alert.SetPositiveButton ("Set", (s, EventLog) => {
				//				AlertDialog dialog = (AlertDialog)dialog;
				speechListenLength = threshold.Value;
				//				speechListenLength = args.Which;
			});
			//			alert.SetView (LayoutInflater.Inflate (Resource.Layout.add_step, null));
			alert.Create ().Show();
		}

		private static bool toggleVoice = false;

		private static void startVoice(){
			toggleVoice = !toggleVoice;

			if (toggleVoice) {
				menu.FindItem (Resource.Id.action_voice).SetTitle (Constants.VoiceEnabled);
				speechService = new SpeechRecognitionService ();
				Java.Lang.JavaSystem.LoadLibrary ("pocketsphinx_jni");
				speechService.Init ();
//				Toast.MakeText (this, "Voice Services Activated", ToastLength.Short).Show ();

				speech.Start ();

			} else {
				menu.FindItem (Resource.Id.action_voice).SetTitle (Constants.VoiceDisabled);
				speechService.stopListening ();
				speech.Stop ();
//				Toast.MakeText (this, "Voice Services Deactivated", ToastLength.Short).Show ();
				//				StopService (new Intent (this, typeof(voiceService)));
			}
		}

		private static async void pocketsphinx (object source, ElapsedEventArgs e){
			speech.Stop ();
			if (!toggleVoice) {
				speechService.stopListening ();
				return;
			}

			Console.WriteLine ("Start sphinx right here");
			var result = await speechService.ListenAsync (speechListenLength);

			if (result == Constants.KEYPHRASE) {
				//				speech.Stop ();
				recordVoice ();
				//				speech.Start ();
			} else {
				speech.Start ();
			}
		}

		private const int VOICE = 10;

		private static void recordVoice ()
		{
			var voiceIntent = new Intent (RecognizerIntent.ActionRecognizeSpeech);
			voiceIntent.PutExtra (RecognizerIntent.ExtraLanguageModel, RecognizerIntent.LanguageModelFreeForm);
			voiceIntent.PutExtra (RecognizerIntent.ExtraPrompt, "Speak Now");
			voiceIntent.PutExtra (RecognizerIntent.ExtraSpeechInputCompleteSilenceLengthMillis, 1500);
			voiceIntent.PutExtra (RecognizerIntent.ExtraSpeechInputPossiblyCompleteSilenceLengthMillis, 1500);
			voiceIntent.PutExtra (RecognizerIntent.ExtraSpeechInputMinimumLengthMillis, 15000);
			voiceIntent.PutExtra (RecognizerIntent.ExtraMaxResults, 1);
			voiceIntent.PutExtra (RecognizerIntent.ExtraLanguage, Java.Util.Locale.Default);
			textToSpeech.Speak ("Listening", QueueMode.Flush, null);
//			StartActivityForResult (voiceIntent, VOICE);
		}

		private static void On_ActivityResult(int requestCode, Result resultVal, Intent data)
		{
			if (requestCode == VOICE) {
				if (resultVal == Result.Ok) {
					var matches = data.GetStringArrayListExtra (RecognizerIntent.ExtraResults);
//					Toast.MakeText (this, matches [0].ToString (), ToastLength.Short).Show ();
					Console.WriteLine ("Google returned a string of {0}", matches [0]);
					textToSpeech.Speak (matches [0], QueueMode.Flush, null);
					//					var result = FindViewById<TextView>(Resource.Id.ResultText);
					//					result.Text = matches[0].ToString();
					speech.Start ();
				}
			}
//			base.OnActivityResult(requestCode, resultVal, data);
		}

//		// Interface method required for IOnInitListener
//		void static TextToSpeech.IOnInitListener.OnInit(OperationResult status)
//		{
//			lang = Java.Util.Locale.Default;
//			// if we get an error, default to the default language
//			if (status == OperationResult.Error)
//				textToSpeech.SetLanguage(Java.Util.Locale.Default);
//			// if the listener is ok, set the lang
//			if (status == OperationResult.Success)
//				textToSpeech.SetLanguage(lang);
//		}
	}
}