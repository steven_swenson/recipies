﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

using Android.Widget;
using Android.Content;
using Android.Database;
using Android.Graphics;
using Android.Util;
using Android.Views;
using Android.OS;

using Org.Json;

using Newtonsoft.Json;

namespace RecipeAssistant
{
	public class RecipeListAdapter : Java.Lang.Object, IListAdapter
	{
		private const string Tag = "RecipleListAdapter";

		public class Item : Java.Lang.Object
		{
			public string Title, Name, Summary;
			public Bitmap Image;
		}

		List<Item> items = new List<Item>();
		Context context;
		DataSetObserver observer;

		public RecipeListAdapter (Context context, List<string> recipesToLoad = null)
		{
			this.context = context;
			LoadRecipeList (recipesToLoad);
		}

		public static void DeleteRecipe(string recipeName){
			List<string> tableOfContents = new List<string> ();
			tableOfContents = GetTableOfContents ();

			tableOfContents.Remove (recipeName);
			SaveTableOfContents (tableOfContents);

			string path = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
			string filePath = System.IO.Path.Combine (path, recipeName);
			System.IO.File.Delete (filePath);

		}

		void LoadRecipeList(List<string> recipesToLoad = null) {
			//Load the table of Contents
			List<string> tableOfContents = new List<string> ();

			try{
//				string toc;
//				using (var file = System.IO.File.Open(filePath, FileMode.Open, FileAccess.Read))
//				using (var strm = new StreamReader(file))
//				{
//					toc = strm.ReadToEnd();
//				}
//
//				tableOfContents = JsonConvert.DeserializeObject<List<string> > (toc);

				tableOfContents = GetTableOfContents();

				if(recipesToLoad != null){
//					foreach( List<string> keep in tableOfContents){
//						if(tableOfContents.
//					}
					var results = tableOfContents.Intersect(recipesToLoad).ToList();
					tableOfContents = results;
				}

			}catch {
				string path = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
				string fileName = Constants.TableOfContents;
				string filePath = System.IO.Path.Combine (path, fileName);

				string json = JsonConvert.SerializeObject(tableOfContents);

				using (var file = File.Open(filePath, FileMode.Create, FileAccess.Write))
				using (var strm = new StreamWriter(file))
				{
					strm.Write(json);
				}
			}

			List<Item> items = new List<Item> ();
			for(int i = 0; i < tableOfContents.Count; i++){
//				string filePath = System.IO.Path.Combine (path, tableOfContents[i]); 
//				string temp;
//				try{
//					using (var file = System.IO.File.Open(filePath, FileMode.Open, FileAccess.Read))
//					using (var strm = new StreamReader(file))
//					{
//						temp = strm.ReadToEnd();
//					}
//					Recipe temp1 = JsonConvert.DeserializeObject<Recipe> (temp);
//					Item item = new Item ();
//					item.Name = temp1.TitleText + ".json";
//					item.Summary = temp1.SummaryText;
//					item.Title = temp1.TitleText;
//
//					//TODO: Load picture if it exists by the same name as the recipe but with .jpeg
//
//					items.Add (item);
//				}catch{
//				};
				Item item = GetRecipeItem (tableOfContents [i]);
				if (item.Title != null) {
					items.Add (item);
				} else {
					DeleteRecipe (tableOfContents [i]);
				}
			}
  			AppendItemsToList (items);

//			JSONObject jsonObject = AssetUtils.LoadJSONAsset (context, Constants.RecipeListFile);
//			if (jsonObject != null) {
//				List<Item> items = ParseJson (jsonObject);
//				AppendItemsToList (items);
//			}
		}

		List<Item> ParseJson(JSONObject json)
		{
			List<Item> result = new List<Item> ();
			try 
			{
				JSONArray items = json.GetJSONArray(Constants.RecipeFieldList);
				for (int i = 0; i < items.Length(); i ++)
				{
					JSONObject item = items.GetJSONObject(i);
					Item parsed = new Item();
					parsed.Name = item.GetString(Constants.RecipeFieldName);
					parsed.Title = item.GetString(Constants.RecipeFieldTitle);
					if (item.Has(Constants.RecipeFieldImage)) {
						String imageFile = item.GetString(Constants.RecipeFieldImage);
						parsed.Image = AssetUtils.LoadBitmapAsset(context, imageFile);
					}
					parsed.Summary = item.GetString(Constants.RecipeFieldSummary);
					result.Add(parsed);
				}
			} catch (Exception ex) {
				Log.Error (Tag, "Failed to parse recipe list: " + ex);
			}
			return result;
		}

		public void AppendItemsToList(List<Item> items)
		{
			this.items.AddRange (items);
			if (observer != null) {
				observer.OnChanged ();
			}
		}

		public bool AreAllItemsEnabled ()
		{
			return true;
		}

		public bool IsEnabled (int position)
		{
			return true;
		}

		public Java.Lang.Object GetItem (int position)
		{
			return items [position];
		}

		public long GetItemId (int position)
		{
			return 0;
		}

		public int GetItemViewType (int position)
		{
			return 0;
		}

		public Android.Views.View GetView (int position, Android.Views.View convertView, Android.Views.ViewGroup parent)
		{
			var view = convertView;
			if (view == null) {
				var inf = LayoutInflater.From (context);
				view = inf.Inflate (Resource.Layout.list_item, null);
			}
			var item = (Item)GetItem (position);
			var titleView = (TextView)view.FindViewById (Resource.Id.textTitle);
			var summaryView = (TextView)view.FindViewById (Resource.Id.textSummary);
			var iv = (ImageView)view.FindViewById (Resource.Id.imageView);

			titleView.Text = item.Title;
			summaryView.Text = item.Summary;
			if (item.Image != null) {
				iv.SetImageBitmap (item.Image);
			} else {
				iv.SetImageDrawable (context.Resources.GetDrawable (Resource.Drawable.ic_noimage));
			}
			return view;
		}

		public void RegisterDataSetObserver (DataSetObserver _observer)
		{
			observer = _observer;
		}

		public void UnregisterDataSetObserver (DataSetObserver observer)
		{
			observer = null;
		}

		public int Count {
			get {
				return items.Count;
			}
		}

		public bool HasStableIds {
			get {
				return false;
			}
		}

		public bool IsEmpty {
			get {
				return items.Count == 0;
			}
		}

		public int ViewTypeCount {
			get {
				return 1;
			}
		}

		public String GetItemName(int position) {
			return items [position].Name;
		}

		public static List<string> GetTableOfContents(){
			string path = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
			string fileName = Constants.TableOfContents;
			string filePath = System.IO.Path.Combine (path, fileName); 

			List<string> tableOfContents = new List<string> ();

			string toc;
			using (var file = System.IO.File.Open(filePath, FileMode.Open, FileAccess.Read))
			using (var strm = new StreamReader(file))
			{
				toc = strm.ReadToEnd();
			}

			tableOfContents = JsonConvert.DeserializeObject<List<string> > (toc);
			return tableOfContents;
		}

		public static void SaveTableOfContents( List<string> toc){
			string path = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
			string fileName = Constants.TableOfContents;
			string filePath = System.IO.Path.Combine (path, fileName); 

			var json = JsonConvert.SerializeObject(toc);

			using (var file = File.Open(filePath, FileMode.Create, FileAccess.Write))
			using (var strm = new StreamWriter(file))
			{
				strm.Write(json);
			}
		}

		public static Recipe GetRecipe(string recipeName){
			string path = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
			string filePath = System.IO.Path.Combine (path, recipeName); 
			string temp;
			try{
				using (var file = System.IO.File.Open(filePath, FileMode.Open, FileAccess.Read))
				using (var strm = new StreamReader(file))
				{
					temp = strm.ReadToEnd();
				}
				Recipe temp1 = JsonConvert.DeserializeObject<Recipe> (temp);

				//TODO: Load picture if it exists by the same name as the recipe but with .jpeg

				return temp1;
			}catch (Exception ex){
				Console.WriteLine ("Error: {0}", ex);
				return new Recipe ();
			};
		}

		public Item GetRecipeItem(string recipeName){
			try{
				Recipe temp1 = GetRecipe(recipeName);
				Item item = new Item ();
				item.Name = temp1.TitleText + ".json";
				item.Summary = temp1.SummaryText;
				item.Title = temp1.TitleText;

				var _dir = new Java.IO.File( Android.OS.Environment.GetExternalStoragePublicDirectory(
					Android.OS.Environment.DirectoryPictures), Constants.AppName.ToString());
				string dir = System.IO.Path.Combine (_dir.AbsolutePath, item.Title + ".jpg");

				item.Image = MainActivity.LoadAndResizeBitmap(dir, 300,300);

				return item;
				}catch (Exception ex){
					Console.WriteLine ("Error: {0}", ex);
					return new Item ();
				};
		}

		public static void SaveRecipe( Recipe recipe ){
			string json = JsonConvert.SerializeObject(recipe);

			string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			string filePath = System.IO.Path.Combine(path, recipe.TitleText + ".json");
			using (var file = File.Open(filePath, FileMode.Create, FileAccess.Write))
			using (var strm = new StreamWriter(file))
			{
				strm.Write(json);
			}
		}
	}
}

