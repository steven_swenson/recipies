﻿using System;

using Android.Widget;
using Android.Speech;
using Android.OS;
using Android.App;
using Android.Content;

using Edu.Cmu;

namespace RecipeAssistant
{
	[Service]
	public class voiceService : Android.App.Service
	{
		System.Threading.Timer _timer;

		public override void OnStart(Android.Content.Intent intent, int startId){
			base.OnStart (intent, startId);

			Java.Lang.JavaSystem.LoadLibrary ("pocketsphinx_jni");
			var config = Edu.Cmu.Pocketsphinx.Decoder.DefaultConfig ();

			DoStuff ();
		}

		public override void OnDestroy ()
		{
			base.OnDestroy ();

			_timer.Dispose ();

			//			Log.Debug ("SimpleService", "SimpleService stopped");      
		}

		public void DoStuff ()
		{
			_timer = new System.Threading.Timer ((o) => {
				//				Log.Debug ("SimpleService", "hello from simple service");
				Console.WriteLine("In service");
				//				var intent = new Intent("RecipeAssistant.recipeAssistant.voiceService");
				//				intent.PutExtra("key", "value");
				//				SendBroadcast(intent);
			}, null, 0, 4000);
		}

		public override Android.OS.IBinder OnBind (Android.Content.Intent intent)
		{
			throw new NotImplementedException ();
		}


		public voiceService ()
		{
//			IntentFilter voiceFilter = new IntentFilter ();
//			voiceFilter.AddAction ("RecipeAssistant.recipeAssistant.voiceService");
//			var voiceFilterReceiver = new voiceReceiver ();
//
//			RegisterReceiver (voiceFilterReceiver, voiceFilter);
		}

	}
}

//[BroadcastReceiver (Enabled = true)]
//[IntentFilter (new[] { "RecipeAssistant.recipeAssistant.voiceService" })]
//public class voiceReceiver : BroadcastReceiver{
//	public override void OnReceive(Context context, Intent intent){
//		string temp = "";
//	}
//
//}
