#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

[assembly: global::Android.Runtime.ResourceDesignerAttribute("RecipeAssistant.Resource", IsApplication=true)]

namespace RecipeAssistant
{
	
	
	[System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Android.Build.Tasks", "1.0.0.0")]
	public partial class Resource
	{
		
		static Resource()
		{
			global::Android.Runtime.ResourceIdManager.UpdateIdValues();
		}
		
		public static void UpdateIdValues()
		{
		}
		
		public partial class Attribute
		{
			
			static Attribute()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Attribute()
			{
			}
		}
		
		public partial class Color
		{
			
			// aapt resource value: 0x7f050001
			public const int list_image_bg_color = 2131034113;
			
			// aapt resource value: 0x7f050000
			public const int list_text_bg_color = 2131034112;
			
			static Color()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Color()
			{
			}
		}
		
		public partial class Drawable
		{
			
			// aapt resource value: 0x7f020000
			public const int ic_noimage = 2130837504;
			
			// aapt resource value: 0x7f020001
			public const int Icon = 2130837505;
			
			static Drawable()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Drawable()
			{
			}
		}
		
		public partial class Id
		{
			
			// aapt resource value: 0x7f09001b
			public const int action_add = 2131296283;
			
			// aapt resource value: 0x7f09001e
			public const int action_export = 2131296286;
			
			// aapt resource value: 0x7f09001d
			public const int action_import = 2131296285;
			
			// aapt resource value: 0x7f09001c
			public const int action_options = 2131296284;
			
			// aapt resource value: 0x7f09001a
			public const int action_search = 2131296282;
			
			// aapt resource value: 0x7f090019
			public const int action_voice = 2131296281;
			
			// aapt resource value: 0x7f090001
			public const int btn_add_ingr = 2131296257;
			
			// aapt resource value: 0x7f09000e
			public const int btn_add_picture = 2131296270;
			
			// aapt resource value: 0x7f090007
			public const int btn_add_step = 2131296263;
			
			// aapt resource value: 0x7f090012
			public const int btn_edit_summary = 2131296274;
			
			// aapt resource value: 0x7f090010
			public const int btn_edit_title = 2131296272;
			
			// aapt resource value: 0x7f090002
			public const int btn_remove_ingr = 2131296258;
			
			// aapt resource value: 0x7f090009
			public const int btn_remove_step = 2131296265;
			
			// aapt resource value: 0x7f09000a
			public const int imageView = 2131296266;
			
			// aapt resource value: 0x7f090013
			public const int ingredientsHeader = 2131296275;
			
			// aapt resource value: 0x7f090016
			public const int layoutSteps = 2131296278;
			
			// aapt resource value: 0x7f090005
			public const int list_ingr = 2131296261;
			
			// aapt resource value: 0x7f090006
			public const int list_step = 2131296262;
			
			// aapt resource value: 0x7f09000d
			public const int recipeImageView = 2131296269;
			
			// aapt resource value: 0x7f090011
			public const int recipeTextSummary = 2131296273;
			
			// aapt resource value: 0x7f09000f
			public const int recipeTextTitle = 2131296271;
			
			// aapt resource value: 0x7f090017
			public const int stepImageView = 2131296279;
			
			// aapt resource value: 0x7f090015
			public const int stepsHeader = 2131296277;
			
			// aapt resource value: 0x7f090014
			public const int textIngredients = 2131296276;
			
			// aapt resource value: 0x7f090018
			public const int textStep = 2131296280;
			
			// aapt resource value: 0x7f09000c
			public const int textSummary = 2131296268;
			
			// aapt resource value: 0x7f09000b
			public const int textTitle = 2131296267;
			
			// aapt resource value: 0x7f090000
			public const int txt_ingr = 2131296256;
			
			// aapt resource value: 0x7f090008
			public const int txt_step = 2131296264;
			
			// aapt resource value: 0x7f090004
			public const int txt_summary = 2131296260;
			
			// aapt resource value: 0x7f090003
			public const int txt_title = 2131296259;
			
			static Id()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Id()
			{
			}
		}
		
		public partial class Layout
		{
			
			// aapt resource value: 0x7f040000
			public const int add_ingr = 2130968576;
			
			// aapt resource value: 0x7f040001
			public const int add_recipe = 2130968577;
			
			// aapt resource value: 0x7f040002
			public const int add_step = 2130968578;
			
			// aapt resource value: 0x7f040003
			public const int list_item = 2130968579;
			
			// aapt resource value: 0x7f040004
			public const int recipe = 2130968580;
			
			// aapt resource value: 0x7f040005
			public const int step_item = 2130968581;
			
			static Layout()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Layout()
			{
			}
		}
		
		public partial class Menu
		{
			
			// aapt resource value: 0x7f080000
			public const int list_options = 2131230720;
			
			// aapt resource value: 0x7f080001
			public const int main = 2131230721;
			
			static Menu()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Menu()
			{
			}
		}
		
		public partial class Mipmap
		{
			
			// aapt resource value: 0x7f030000
			public const int ic_app_recipe = 2130903040;
			
			// aapt resource value: 0x7f030001
			public const int ic_notification_recipe = 2130903041;
			
			static Mipmap()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Mipmap()
			{
			}
		}
		
		public partial class String
		{
			
			// aapt resource value: 0x7f060008
			public const int action_add = 2131099656;
			
			// aapt resource value: 0x7f060007
			public const int action_export = 2131099655;
			
			// aapt resource value: 0x7f060006
			public const int action_import = 2131099654;
			
			// aapt resource value: 0x7f060002
			public const int action_options = 2131099650;
			
			// aapt resource value: 0x7f060003
			public const int action_search = 2131099651;
			
			// aapt resource value: 0x7f060004
			public const int action_settings = 2131099652;
			
			// aapt resource value: 0x7f060005
			public const int action_voice = 2131099653;
			
			// aapt resource value: 0x7f060001
			public const int app_add = 2131099649;
			
			// aapt resource value: 0x7f060000
			public const int app_name = 2131099648;
			
			// aapt resource value: 0x7f06000d
			public const int delete_recipe = 2131099661;
			
			// aapt resource value: 0x7f06000a
			public const int ingredients = 2131099658;
			
			// aapt resource value: 0x7f06000e
			public const int modify_recipe = 2131099662;
			
			// aapt resource value: 0x7f06000b
			public const int step_count = 2131099659;
			
			// aapt resource value: 0x7f060009
			public const int steps = 2131099657;
			
			// aapt resource value: 0x7f06000c
			public const int table_of_contents = 2131099660;
			
			static String()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private String()
			{
			}
		}
		
		public partial class Style
		{
			
			// aapt resource value: 0x7f070001
			public const int RecipeHugeFont = 2131165185;
			
			// aapt resource value: 0x7f070002
			public const int RecipeSummaryFont = 2131165186;
			
			// aapt resource value: 0x7f070000
			public const int RecipeTitleFont = 2131165184;
			
			static Style()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Style()
			{
			}
		}
	}
}
#pragma warning restore 1591
